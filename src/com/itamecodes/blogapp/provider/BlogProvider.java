package com.itamecodes.blogapp.provider;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.ContentProvider;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.OperationApplicationException;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;

/**
 * The content provider which handles everything that we do with our database 
 * I have declared the table also in the content provider , but in real world it is better to have the tables as separate class with which 
 * our content provider establishes a contract
 * @author vivek
 *
 */
public class BlogProvider extends ContentProvider {
	public static final String AUTHORITY="com.itamecodes.blogapp.provider.BlogProvider";
	public static final String URL="content://"+AUTHORITY+"/news";
	public static final Uri CONTENT_URI=Uri.parse(URL);
	
	//the columns of the table
	public static final String ID="_id";
	public static final String TITLE="title";
	public static final String CONTENT="content";
	
	// constants for the uri matcher
	private static final int NEWS=1;
	private static final int NEWS_ID=2;
	//the db helper
	private static DBHelper dbHelper;
	
	private static HashMap<String,String> NewsMap;
	
	//handle the urimatches today
	static final UriMatcher uriMatcher;
	static{
		uriMatcher=new UriMatcher(UriMatcher.NO_MATCH);
		uriMatcher.addURI(AUTHORITY, "news", NEWS);
		uriMatcher.addURI(AUTHORITY, "news/#", NEWS_ID);
	}
	
	private SQLiteDatabase database;
	
	static final String DATABASE_NAME="NewsDatabase";
	static final String TABLE_NAME="NewsTable";
	//the database version set as 1, incrementing this will take care of our DB update
	static final int DATABASE_VERSION=1;
	//sql for table creation
	static final String CREATE_TABLE=" CREATE TABLE " + TABLE_NAME +" ("+ID+" INTEGER PRIMARY KEY AUTOINCREMENT," +TITLE+" TEXT NOT NULL," +CONTENT+" TEXT NOT NULL);";
	
	private static final String CONTENT_TYPE=ContentResolver.CURSOR_DIR_BASE_TYPE+"/vnd.itamecodes.news";
	private static final String CONTENT_ITEM_TYPE=ContentResolver.CURSOR_ITEM_BASE_TYPE+"/vnd.itamecodes.news";
	
	private static class DBHelper extends SQLiteOpenHelper{

		public DBHelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
		}
		
		public void onCreate(SQLiteDatabase db){
			db.execSQL(CREATE_TABLE);
		}
		
		public void onUpgrade(SQLiteDatabase db,int oldVersion,int newVersion){
            db.execSQL("DROP TABLE IF EXISTS " +  TABLE_NAME);
            onCreate(db);
            
		}
		
	}

	
	@Override
	public boolean onCreate() {
		Context context=getContext();
		dbHelper=new DBHelper(context);
		database=dbHelper.getWritableDatabase();
		if(database==null){
			return false;
		}else{
			return true;
		}
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
            String[] selectionArgs, String sortOrder) {
		SQLiteQueryBuilder queryBuilder=new SQLiteQueryBuilder();
		queryBuilder.setTables(TABLE_NAME);
		switch(uriMatcher.match(uri)){
			case NEWS:{
				queryBuilder.setProjectionMap(NewsMap);
				break;
			}
			case NEWS_ID:{
				//we wont use this in our app 
				queryBuilder.appendWhere(ID+"="+uri.getLastPathSegment());
				break;
			}
			default:{
				throw new IllegalArgumentException("Please check the uri");
			}
			
		}
		if(sortOrder==null || sortOrder==""){
			sortOrder=TITLE;
		}
		Cursor cursor=queryBuilder.query(database, projection, selection, selectionArgs, null, null, sortOrder);
		//Notify the cursorloader that things have changed in the content provider, now please let the adapter know of it
		cursor.setNotificationUri(getContext().getContentResolver(), uri);
		return cursor;
	}
	
	@Override
	public Uri insert(Uri uri, ContentValues values) {
		long row = database.insert(TABLE_NAME, "", values);
		if(row > 0) {
			Uri newUri = ContentUris.withAppendedId(CONTENT_URI, row);
			//Notify the cursorloader that things have changed in the content provider, now please let the adapter know of it
			getContext().getContentResolver().notifyChange(newUri, null);
			return newUri;
		}
		throw new SQLException("Fail to add a new record into " + uri);

	}
	
	


	
	
	@Override
	public ContentProviderResult[] applyBatch(
	        ArrayList<ContentProviderOperation> operations) {
	    ContentProviderResult[] result = new ContentProviderResult[operations
	            .size()];
	    int i = 0;
	    // Opens the database object in "write" mode.
	    // Begin a transaction
	    database.beginTransaction();
	    try {
	        for (ContentProviderOperation operation : operations) {
	            // Chain the result for back references
	            result[i++] = operation.apply(this, result, i);
	        }

	        database.setTransactionSuccessful();
	    } catch (OperationApplicationException e) {
	    } finally {
	        database.endTransaction();
	    }

	    return result;
	}

	@Override
	 public int update(Uri uri, ContentValues values, String selection,
	             String[] selectionArgs) {
	      int count = 0;
	      switch (uriMatcher.match(uri)){
	           case NEWS:
	              count = database.update(TABLE_NAME, values, selection, selectionArgs);
	              break;
	           case NEWS_ID:
	              count = database.update(TABLE_NAME, values, ID +" = " + uri.getLastPathSegment() +
	                      (!TextUtils.isEmpty(selection) ? " AND (" +
	                      selection + ')' : ""), selectionArgs);
	              break;
	           default:
	              throw new IllegalArgumentException("Unsupported URI " + uri );
	        }
	        getContext().getContentResolver().notifyChange(uri, null);
	        return count;
	     }
	 
	 @Override
		public int delete(Uri uri, String selection, String[] selectionArgs) {
			int count = 0;
			switch (uriMatcher.match(uri)){
		      case NEWS:
		    	  // delete all the records of the table
		    	  count = database.delete(TABLE_NAME, selection, selectionArgs);
		    	  break;
		      case NEWS_ID:
		      	  String id = uri.getLastPathSegment();	//gets the id
		          count = database.delete( TABLE_NAME, ID +  " = " + id + 
		                (!TextUtils.isEmpty(selection) ? " AND (" + 
		                selection + ')' : ""), selectionArgs);
		          break;
		      default: 
		          throw new IllegalArgumentException("Unsupported URI " + uri);
		      }
		      getContext().getContentResolver().notifyChange(uri, null);
		      return count;
			
			
		}

		@Override
		public String getType(Uri uri) {
			switch (uriMatcher.match(uri)){
		      case NEWS:
		         return CONTENT_TYPE;
		      case NEWS_ID:
		         return CONTENT_ITEM_TYPE;
		      default:
		         throw new IllegalArgumentException("Unsupported URI: " + uri);
		      }
		}

	      


}
