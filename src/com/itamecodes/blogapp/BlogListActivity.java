package com.itamecodes.blogapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.Window;
import android.widget.Toast;

import com.itamecodes.blogapp.gsonobjects.Post;
import com.itamecodes.blogapp.networkhandler.DataLoadingService;
import com.itamecodes.blogapp.utils.EventBus;
import com.itamecodes.blogapp.utils.SignalLoadOver;
import com.squareup.otto.Subscribe;

/**
 * An activity representing a list of Items. This activity has different
 * presentations for handset and tablet-size devices. On handsets, the activity
 * presents a list of items, which when touched, lead to a
 * {@link ItemDetailActivity} representing item details. On tablets, the
 * activity presents the list of items and item details side-by-side using two
 * vertical panes.
 * <p>
 * The activity makes heavy use of fragments. The list of items is a
 * {@link ItemListFragment} and the item details (if present) is a
 * {@link ItemDetailFragment}.
 * <p>
 * This activity also implements the required {@link ItemListFragment.Callbacks}
 * interface to listen for item selections.
 */
public class BlogListActivity extends ActionBarActivity{

	/**
	 * Whether or not the activity is in two-pane mode, i.e. running on a tablet
	 * device.
	 */
	private boolean mTwoPane;
	public static final String NEWS_SYNC_STATUS="isNewsSynced";
	private Handler mHandler=new Handler();
	/**
	 * We create a runnable, to refresh the content after every 30 minutes
	 */
	private Runnable syncDataATTimeInterval=new Runnable() {
	    public void run() {
	        // do real work here
	    	Intent msgIntent = new Intent(BlogListActivity.this, DataLoadingService.class);
			startService(msgIntent);
	        mHandler.postDelayed(syncDataATTimeInterval,30*60*1000);
	    }
	};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		supportRequestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		getSupportActionBar().setDisplayShowTitleEnabled(false);
		
		setContentView(R.layout.activity_item_list);
		 mHandler.postDelayed(syncDataATTimeInterval, 30*60*1000);
		if(!loadSavedPreferences()){
			Intent msgIntent = new Intent(this, DataLoadingService.class);
			startService(msgIntent);
		}
		
		
		if (findViewById(R.id.item_detail_container) != null) {
			// The detail container view will be present only in the
			// large-screen layouts (res/values-large and
			// res/values-sw600dp). If this view is present, then the
			// activity should be in two-pane mode.
			mTwoPane = true;
			// In two-pane mode, list items should be given the
			// 'activated' state when touched.
			
		}
		((BlogListFragment) getSupportFragmentManager().findFragmentById(
				R.id.item_list)).setActivateOnItemClick(true);
		
	}
	
	/**
	 * Once the data is synced , the status of sync is saved in the shared prefs .
	 * This method returns true or false, depending on whwther the data is synced or not
	 * @return
	 */
	private boolean loadSavedPreferences() {
		SharedPreferences sharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(this);
		boolean isNewsSynced=sharedPreferences.getBoolean(NEWS_SYNC_STATUS, false);
		return isNewsSynced;
	}
	
	
	/**
	 * This is the subscriber to the event bus,it is notified that the user has selected a blog in the list and it recieves the Post object
	 * @param post
	 */
	@Subscribe
	public void onPostClicked(Post post) {
		if (!mTwoPane) {
			/**
			 * if this is not a two pane layout put the required details to be displayed in an intent and send to the 
			 * activity hosting the detailfragment
			 */
			Intent detailIntent = new Intent(this, BlogDetailActivity.class);
			detailIntent.putExtra("post", post);
			startActivity(detailIntent);
		} else {
			/**
			 * if it is a two pane layout get a new instance of the frgamnet and replace it into the container
			 */
			BlogDetailFragment fragment = BlogDetailFragment.newInstance(post);
			getSupportFragmentManager().beginTransaction()
					.replace(R.id.item_detail_container, fragment).commit();
			
					
		}

	}
	
	/**
	 * This is the event bus subscriber. When the refresh of data is done, remove the indeterminate progressbar 
	 * @param signalLoadOver
	 */
	@Subscribe
	public void onSignalLoadOver(SignalLoadOver signalLoadOver){
		setSupportProgressBarIndeterminateVisibility(false);
	}
	
	@Override
	  public boolean onCreateOptionsMenu(Menu menu) {
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.menu, menu);
	    return true;
	  } 
	
	@Override
	  public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	    // action with ID action_refresh was selected
	    case R.id.action_refresh:
	    	if(isNetworkOnline()){
	    		//if network is connected, restart the service and show indeterminateprogressbar
		    	setSupportProgressBarIndeterminateVisibility(true);
		    	Intent msgIntent = new Intent(this, DataLoadingService.class);
				startService(msgIntent);
	    	}else{
	    		Toast.makeText(this,R.string.refreshonnetworkerror, Toast.LENGTH_LONG).show();
	    	}
	    	break;
	    default:
	      break;
	    }

	    return true;
	  }
	
	/**
	 * Register the event bus
	 */
	@Override
	public void onResume() {
		super.onResume();
		EventBus.getInstance().register(this);
	}

	/**
	 * Unregister the event bus
	 */
	@Override
	public void onPause() {
		EventBus.getInstance().unregister(this);
		super.onPause();
	}

	/**
	 * Checks whether the device is connected to the internet
	 * @return
	 */
	public boolean isNetworkOnline() {
	    boolean status=false;
	    try{
	        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
	        NetworkInfo netInfo = cm.getNetworkInfo(0);
	        if (netInfo != null && netInfo.getState()==NetworkInfo.State.CONNECTED) {
	            status= true;
	        }else {
	            netInfo = cm.getNetworkInfo(1);
	            if(netInfo!=null && netInfo.getState()==NetworkInfo.State.CONNECTED)
	                status= true;
	        }
	    }catch(Exception e){
	        return false;
	    }
	    return status;

	    }  

	
}
