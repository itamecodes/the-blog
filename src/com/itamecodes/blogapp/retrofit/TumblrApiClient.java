package com.itamecodes.blogapp.retrofit;

import java.util.TimeZone;

import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;
import retrofit.http.GET;
import retrofit.http.Query;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.itamecodes.blogapp.Config;
import com.itamecodes.blogapp.gsonobjects.JsonReturned;

/**
 * The basic bootstrapping code of retrofit. We set up the rest adapter client and the interface necessary for retrofit to 
 * do its job well
 * @author vivek
 *
 */
public class TumblrApiClient {
	
	private static TumblrApiInterface mTumblrApiInterface;
	
	public static TumblrApiInterface getTumblrApiClient(){
		
		if(mTumblrApiInterface==null){
			TimeZone tz=TimeZone.getDefault();
			Gson gson = new GsonBuilder()
			.setDateFormat("yyyy-MM-dd HH:mm:ss Z")
			.create();
			RestAdapter restAdapter=new RestAdapter.Builder().setEndpoint(Config.RESTURL).setConverter(new GsonConverter(gson)).build();
			mTumblrApiInterface=restAdapter.create(TumblrApiInterface.class);
		}
		
		return mTumblrApiInterface;
	}
	
	public interface TumblrApiInterface{
		@GET("/posts")
		JsonReturned getPosts(@Query("api_key") String apiKey,@Query("limit") int limit);
	}

	
}
