package com.itamecodes.blogapp;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.itamecodes.blogapp.gsonobjects.Post;

/**
 * A fragment representing a single Item detail screen. This fragment is either
 * contained in a {@link ItemListActivity} in two-pane mode (on tablets) or a
 * {@link ItemDetailActivity} on handsets.
 */
public class BlogDetailFragment extends Fragment {
	/**
	 * The fragment argument representing the item ID that this fragment
	 * represents.
	 */
	public static final String ARG_ITEM_ID = "item_id";

	private Typeface mType;
	
	/**
	 * Mandatory empty constructor for the fragment manager to instantiate the
	 * fragment (e.g. upon screen orientation changes).
	 */
	public BlogDetailFragment() {
	}

	public static BlogDetailFragment newInstance(Post post){
		BlogDetailFragment itemDetailFragment=new BlogDetailFragment();
		Bundle bundle=new Bundle();
		bundle.putParcelable("post", post);
		itemDetailFragment.setArguments(bundle);
		return itemDetailFragment;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//getArguments().getParcelable("post");
		 mType = Typeface.createFromAsset(getActivity().getResources().getAssets(),"fonts/Roboto/Roboto/Roboto-Regular.ttf"); 
			

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_item_detail,
				container, false);
		Post post=getArguments().getParcelable("post");
		// Show the dummy content as text in a TextView.
		//if (mItem != null) {
		TextView contentTextView=((TextView) rootView.findViewById(R.id.item_detail));
		contentTextView.setTypeface(mType);
		contentTextView.setText(Html.fromHtml(post.body).toString());
		//}
			
		return rootView;
	}
}
