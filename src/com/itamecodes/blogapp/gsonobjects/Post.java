package com.itamecodes.blogapp.gsonobjects;

import java.util.Date;

import android.os.Parcel;
import android.os.Parcelable;
/**
 * We make the class implement parcelable because we would need to pass this object around between activities
 * @author vivek
 *
 */
public class Post implements Parcelable{
	public String title;
	public String body;
	public Date date;
	private long publishDate;
	
	public Post(String lTitle,String lBody){
		title=lTitle;
		body=lBody;
	}
	
	public Post(Parcel source) {
		title = source.readString();
		body=source.readString();
		//publishDate=source.readLong();
	}
	public void setPublishDate(Date date){
		publishDate=date.getTime();
	}
	public Date getPublishDate(){
		return new Date(publishDate);
	}
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(title);
		dest.writeString(body);
		//dest.writeLong(publishDate);
	}

	@Override
	public int describeContents() {
		return 0;
	}
	
	
	public static final Creator<Post> CREATOR = new Parcelable.Creator<Post>() {

		public Post createFromParcel(Parcel source) {
			return new Post(source);
		}

		public Post[] newArray(int size) {
			return new Post[size];
		}
	};
	

}
