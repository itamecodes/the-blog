package com.itamecodes.blogapp;

import android.content.Context;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.itamecodes.blogapp.adapters.BlogAdapter;
import com.itamecodes.blogapp.gsonobjects.Post;
import com.itamecodes.blogapp.provider.BlogProvider;
import com.itamecodes.blogapp.utils.EventBus;
import com.itamecodes.blogapp.utils.SignalLoadOver;

/**
 * A list fragment representing a list of Items. This fragment also supports
 * tablet devices by allowing list items to be given an 'activated' state upon
 * selection. This helps indicate which item is currently being viewed in a
 * {@link ItemDetailFragment}.
 * <p>
 * Activities containing this fragment MUST implement the {@link Callbacks}
 * interface.
 */
public class BlogListFragment extends Fragment implements LoaderCallbacks<Cursor>,OnItemClickListener{

	/**
	 * The serialization (saved instance state) Bundle key representing the
	 * activated item position. Only used on tablets.
	 */
	private static final String STATE_ACTIVATED_POSITION = "activated_position";
	
	private static final int LOADER_ID=1;
	private ListView mNewsListView;
	private ProgressBar mProgressBar;
	/**
	 * The current activated item  position. Only used on tablets.
	 */
	private int mActivatedPosition = ListView.INVALID_POSITION;
	
	private BlogAdapter adapter;

	/**
	 * Mandatory empty constructor for the fragment manager to instantiate the
	 * fragment (e.g. upon screen orientation changes).
	 */
	public BlogListFragment() {
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		adapter=new BlogAdapter(getActivity(), null, 0);
		getLoaderManager().initLoader(LOADER_ID, null, this);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View newsTitleFragmentView=inflater.inflate(R.layout.blogtitlelayout,container,false);
		mProgressBar=(ProgressBar)newsTitleFragmentView.findViewById(R.id.theprogressbar);
		mNewsListView=(ListView) newsTitleFragmentView.findViewById(R.id.newstitlelistview);
		mNewsListView.setAdapter(adapter);
		mNewsListView.setOnItemClickListener(this);;
		mProgressBar.setVisibility(View.VISIBLE);
		return newsTitleFragmentView;
	}
	
	

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		// Restore the previously serialized activated item position.
		if (savedInstanceState != null
				&& savedInstanceState.containsKey(STATE_ACTIVATED_POSITION)) {
			setActivatedPosition(savedInstanceState
					.getInt(STATE_ACTIVATED_POSITION));
		}
		
		
	}
	
	
	/**
	 *Register the event bus here 
	 */
	@Override
	public void onResume(){
		super.onResume();
		EventBus.getInstance().register(this);
	}

	/**
	 * Unregister the event bus
	 */
	@Override
	public void onPause(){
		EventBus.getInstance().unregister(this);
		super.onPause();
	}


	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		if (mActivatedPosition != ListView.INVALID_POSITION) {
			// Serialize and persist the activated item position.
			outState.putInt(STATE_ACTIVATED_POSITION, mActivatedPosition);
		}
	}

	/**
	 * Turns on activate-on-click mode. When this mode is on, list items will be
	 * given the 'activated' state when touched.
	 */
	public void setActivateOnItemClick(boolean activateOnItemClick) {
		// When setting CHOICE_MODE_SINGLE, ListView will automatically
		// give items the 'activated' state when touched.
		mNewsListView.setChoiceMode(
				activateOnItemClick ? ListView.CHOICE_MODE_SINGLE
						: ListView.CHOICE_MODE_NONE);
	}

	private void setActivatedPosition(int position) {
		if (position == ListView.INVALID_POSITION) {
			mNewsListView.setItemChecked(mActivatedPosition, false);
		} else {
			mNewsListView.setItemChecked(position, true);
		}

		mActivatedPosition = position;
	}

	/**
	 * Set the cursor loader in action, and ask it to fetch the required content via the projection 
	 */
	@Override
	public Loader<Cursor> onCreateLoader(int arg0, Bundle arg1) {
		String[] projection={BlogProvider.ID,BlogProvider.TITLE,BlogProvider.CONTENT};
		CursorLoader cursorLoader=new CursorLoader(getActivity(),BlogProvider.CONTENT_URI,projection,null,null,null);
		return cursorLoader;
	}

	/**
	 * Once the load is over, display the listview 
	 */
	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
		
		if(data.getCount()>0){
			mProgressBar.setVisibility(View.GONE);
			mNewsListView.setVisibility(View.VISIBLE);
		}else{
			if(!isNetworkOnline()){
				mProgressBar.setVisibility(View.GONE);
				mNewsListView.setVisibility(View.VISIBLE);
				Toast.makeText(getActivity(), R.string.networkerroronload, Toast.LENGTH_LONG).show();
			}
		}
		//send the signal to the main activity that the data is loaded, so that if the progressbarindeterminate is visible, it can be removed
		SignalLoadOver signalLoadOver=new SignalLoadOver();
		EventBus.getInstance().post(signalLoadOver);
		adapter.swapCursor(data);
	}

	@Override
	public void onLoaderReset(Loader<Cursor> arg0) {
		adapter.swapCursor(null);
	}

	
	/**
	 * The item click listener of the listview
	 * Get the item clicked
	 * Get the title and content from the item clicked
	 * Create the Post object
	 * Send it via the event bus to the activity
	 */
	@Override
	public void onItemClick(AdapterView<?> list, View view, int position, long id) {
		mActivatedPosition=position;
		Cursor cursor=(Cursor)list.getItemAtPosition(position);
		String postTitle=cursor.getString(cursor.getColumnIndex(BlogProvider.TITLE));
		String postBody=cursor.getString(cursor.getColumnIndex(BlogProvider.CONTENT));
		Post post=new Post(postTitle,postBody);
		EventBus.getInstance().post(post);
		
	}
	
	/**
	 * Checks whether the device is connected to the internet
	 * @return
	 */
	public boolean isNetworkOnline() {
	    boolean status=false;
	    try{
	        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
	        NetworkInfo netInfo = cm.getNetworkInfo(0);
	        if (netInfo != null && netInfo.getState()==NetworkInfo.State.CONNECTED) {
	            status= true;
	        }else {
	            netInfo = cm.getNetworkInfo(1);
	            if(netInfo!=null && netInfo.getState()==NetworkInfo.State.CONNECTED)
	                status= true;
	        }
	    }catch(Exception e){
	        return false;
	    }
	    return status;

	    }  
}
