package com.itamecodes.blogapp.networkhandler;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.TimeZone;

import android.app.IntentService;
import android.content.ContentProviderOperation;
import android.content.Intent;
import android.content.OperationApplicationException;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.RemoteException;
import android.preference.PreferenceManager;

import com.itamecodes.blogapp.BlogListActivity;
import com.itamecodes.blogapp.Config;
import com.itamecodes.blogapp.gsonobjects.JsonReturned;
import com.itamecodes.blogapp.gsonobjects.Post;
import com.itamecodes.blogapp.gsonobjects.ResponseNet;
import com.itamecodes.blogapp.provider.BlogProvider;
import com.itamecodes.blogapp.retrofit.TumblrApiClient;

/**
 * This intent service is responsible for loading data from the internet
 * and populating the database 
 * It ensures that the entire database is populated in one go via a transaction
 * It uses retrofit to do the network handling and json parsing which retrofit handles internally with the help of gson
 * 
 * @author vivek
 *
 */
public class DataLoadingService extends IntentService{

	public DataLoadingService() {
		super("DataLoadingService");
	}
	
	@Override
	protected void onHandleIntent(Intent intent) {
		boolean dataLoaded=false;
		JsonReturned res=null;
		// we need to populate the database in one transaction, so we store each insert operations in an arraylist , so that they can be applied
		// as a batch operation
		final ArrayList<ContentProviderOperation> operations=new ArrayList<ContentProviderOperation>();
		// go get the data from the internet is  
		try{
			//retrofit client does all the job for us
			res=TumblrApiClient.getTumblrApiClient().getPosts(Config.APIKEY,40);
			dataLoaded=true;
		}catch(Exception e){
			dataLoaded=false;	
		}
		if(dataLoaded){
			ResponseNet resp=res.response;
			TimeZone tz=TimeZone.getDefault();
			DateFormat formatter= new SimpleDateFormat("MM/dd/yyyy HH:mm:ss ");
			formatter.setTimeZone(tz);
			if(resp.posts.size()>0){
				//truncate the database , so that we can sync new data
				operations.add(ContentProviderOperation.newDelete(BlogProvider.CONTENT_URI).build()); 
				savePreferences(BlogListActivity.NEWS_SYNC_STATUS, true);
			}
			for (Post p:resp.posts){
				//for each of the Post object, get the body and title, and send it to the db for insertion
				String title=p.title==null?"":p.title;
				String body=p.body==null?"":p.body;
				operations.add(ContentProviderOperation.newInsert(BlogProvider.CONTENT_URI)
					       .withValue(BlogProvider.TITLE,title)
					       .withValue(BlogProvider.CONTENT, body)
					       .withYieldAllowed(true)
					       .build());
					
			}
			try{
				getContentResolver().applyBatch(BlogProvider.AUTHORITY, operations);
			} catch (final RemoteException e) {
			} catch (final OperationApplicationException e) {
			}catch(Exception r){
			}
		}
		
		
	}
	
	/**
	 * If data is loaded properly, save the status as true in our shared preferences
	 * ideally this should be done after the database has been successfuly populated
	 * @param key
	 * @param value
	 */
	private void savePreferences(String key, boolean value) {
		SharedPreferences sharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(this);
		Editor editor = sharedPreferences.edit();
		editor.putBoolean(key, value);
		editor.commit();
	}

}
