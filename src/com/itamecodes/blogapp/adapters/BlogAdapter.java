package com.itamecodes.blogapp.adapters;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Typeface;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.itamecodes.blogapp.R;
import com.itamecodes.blogapp.provider.BlogProvider;

/**
 * The Custom cursor adapter responsible for displaying data in the listview 
 * 
 * @author vivek
 *
 */
public class BlogAdapter extends CursorAdapter {
	
	private LayoutInflater mLayoutInflater;
	//the fade in animation of the list elements
	private Animation mAnim;
	private Typeface mType;
	
	/**
	 * The constructor of the adapter , we intialize the inflater and the fade in animation here
	 * 
	 * @param context
	 * @param cursor
	 * @param flags
	 */
	public BlogAdapter(Context context,Cursor cursor,int flags){
		super(context,cursor,flags);
		//initialize the inflater
        mLayoutInflater = LayoutInflater.from(context); 
        //load the fade in animation
        mAnim = AnimationUtils.loadAnimation(context, R.anim.fadein);
        //initialize the typeface , we need roboto everywhere
        mType = Typeface.createFromAsset(context.getResources().getAssets(),"fonts/Roboto/Roboto/Roboto-Regular.ttf"); 
	}
	
	@Override
	public View newView(Context context, Cursor cursor, ViewGroup parent) {
			// inflate the row of the listview
	        View v = mLayoutInflater.inflate(R.layout.items_row, parent, false);
	        // start the fade in animation of the view
	        v.startAnimation(mAnim);
	        return v;
	}
	 
	 @Override
	    public void bindView(View view, Context context, Cursor cursor) {
		 	//we take the data from the cursor for the respective column and populate the title textview
	        String title = cursor.getString(cursor.getColumnIndexOrThrow(BlogProvider.TITLE));
	        String content = cursor.getString(cursor.getColumnIndexOrThrow(BlogProvider.CONTENT));
	        TextView title_text = (TextView) view.findViewById(R.id.newstitle);
	        title_text.setTypeface(mType);
	        if (title_text != null) {
	            title_text.setText(title);
	        }

	    }


	
	

}
